package com.lomdaat.youtubevideosapplication.ui;

import android.app.Activity;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.lomdaat.youtubevideosapplication.R;
import com.lomdaat.youtubevideosapplication.YouTubeApplication;
import com.lomdaat.youtubevideosapplication.model.Playlists;

/**
 * A placeholder fragment containing a simple view.
 */
public class PlayListsFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener {
    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    private static final String SECTION_ID = "section_id";
    private static final String SECTION_DATA = "section_data";
    private static final String TAG = PlayListsFragment.class.getName();


    protected SwipeRefreshLayout mSwipeLayout;


    private Playlists mPlayLists;
    private Handler handler;

    protected PlayListsAdapter playListsAdapter;
    private RecyclerView recList;

    private TextView empty;
    private RelativeLayout mainContent;
    private LinearLayoutManager layoutManager;


    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static PlayListsFragment newInstance(String sectionID, String data) {
        PlayListsFragment fragment = new PlayListsFragment();
        Bundle args = new Bundle();
        args.putString(SECTION_ID, sectionID);
        args.putString(SECTION_DATA, data);
        fragment.setArguments(args);
        return fragment;
    }

    public PlayListsFragment() {
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mPlayLists = new Playlists();
        playListsAdapter = new PlayListsAdapter((AppCompatActivity) getActivity(), mPlayLists);
    }

    public void requestsPlayLists() {


        String url = "http://www.razor-tech.co.il/hiring/youtube-api.json";

        Log.d(TAG, "get playlists by url" + url);

        StringRequest req = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        Playlists playlists = (Playlists) GsonProvider.getInstance().parse(response, Playlists.class);
                        mPlayLists = playlists;
                        playListsAdapter.setPlayLists(playlists);


                            /*for (int i = 0; playlists != null && i < playlists.getPlaylists().size(); i++)

							{
								boolean ok = true;

								if (ok) {
									mPlayLists.getPlaylists().add(playlists.getPlaylists().get(i));

									playListsAdapter.notifyItemInserted(mPlayLists.getPlaylists().size() - 1);
								}


							}*/


                        mSwipeLayout.setRefreshing(false);
                        //mSwipeLayout.setEnabled(false);

                        checkEmptyList();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e(TAG, volleyError != null ? volleyError.getLocalizedMessage() : "Error on purchase process");


                if (volleyError != null && volleyError.getClass().equals(NoConnectionError.class)) {
                    showNoConnection();
                } else {
                    showError(getString(R.string.general_error_volly));
                }


            }
        }) {

        };
        // add the request object to the queue to be executed
        YouTubeApplication.getInstance().getRequestQueue().add(req);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_rec_view, container, false);

        handler = new Handler();
        empty = (TextView) rootView.findViewById(R.id.empty);
        mainContent = (RelativeLayout) rootView.findViewById(R.id.main_content);
        empty.setVisibility(View.GONE);
        mSwipeLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_container);
        mSwipeLayout.setOnRefreshListener(this);
        mSwipeLayout.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
//			mSwipeLayout.setEnabled(false);

        // Set up recycler view
        recList = (RecyclerView) rootView.findViewById(R.id.cardList);
        layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recList.setHasFixedSize(true);
        recList.setLayoutManager(layoutManager);
        recList.setAdapter(playListsAdapter);

        //    setRecListByConfig(getResources().getConfiguration());


        if (mPlayLists == null || mPlayLists.getPlaylists().size() == 0) {
            mSwipeLayout.measure(recList.getWidth(), recList.getHeight());
            mSwipeLayout.setRefreshing(true);
            requestsPlayLists();
        }

        return rootView;
    }


    @Override
    public void onRefresh() {
        empty.setVisibility(View.GONE);
        mSwipeLayout.setRefreshing(true);
        mPlayLists.getPlaylists().clear();

        playListsAdapter.notifyDataSetChanged();
        requestsPlayLists();


    }


    private void checkEmptyList() {
        if (mPlayLists == null || mPlayLists.getPlaylists().size() == 0) {

            {
                empty.setText(R.string.list_empty);
            }
            empty.setVisibility(View.VISIBLE);
        } else
            empty.setVisibility(View.GONE);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

    }


    public void setRecListByConfig(Configuration configuration) {


    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {

        setRecListByConfig(newConfig);


        super.onConfigurationChanged(newConfig);
    }


    @Override
    public void onResume() {
        super.onResume();


    }

    @Override
    public void onStop() {

        super.onStop();
    }

    @Override
    public void onDestroy() {

        super.onDestroy();

    }

    @Override
    public void onPause() {

        super.onPause();
    }


    public void retryRequests() {

        onRefresh();
    }


}