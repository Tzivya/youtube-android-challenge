package com.lomdaat.youtubevideosapplication.ui;


import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.lomdaat.youtubevideosapplication.R;
import com.lomdaat.youtubevideosapplication.model.ListItem;
import com.squareup.picasso.Picasso;

import java.util.List;

public class PlayListsItemsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    public AppCompatActivity activity;

    protected List<ListItem> playlistsItems;
    private final int VIEW_ITEM = 1;
    private final int VIEW_PROG = 0;

    public PlayListsItemsAdapter(AppCompatActivity activity, List<ListItem> playlistsItems) {
        this.activity = activity;
        setPlayListItemsList(playlistsItems);
    }

    public PlayListsItemsAdapter() {

    }

    public void setPlayListItemsList(List<ListItem> playlistsItems) {
        this.playlistsItems = playlistsItems;
        notifyDataSetChanged();
    }


    @Override
    public int getItemCount() {
        return playlistsItems.size();
    }


    @Override
    public int getItemViewType(int position) {
        return playlistsItems.get(position) != null ? VIEW_ITEM : VIEW_PROG;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        RecyclerView.ViewHolder vh;
        View itemView = null;


        itemView = LayoutInflater.
                from(viewGroup.getContext()).
                inflate(R.layout.play_lst_item_views, viewGroup, false);

        vh = new PlayListsViewHolder(itemView);

        return vh;


    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof PlayListsViewHolder) {
            PlayListsViewHolder playListsViewHolder = ((PlayListsViewHolder) holder);
            final ListItem playlistsItem = playlistsItems.get(position);
            playListsViewHolder.title.setText(playlistsItem.getTitle());
            playListsViewHolder.link.setText(playlistsItem.getLink());

            Picasso.with(activity).load(playlistsItem.getThumb())
                    .placeholder(R.drawable.no_image_available)
                    .error(R.drawable.no_image_available)
                    .into(playListsViewHolder.thumb);
            playListsViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(playlistsItem.getLink()));

                    activity.startActivity(intent);
                    // here show on youtube
                }
            });


        } else {
            ((ProgressViewHolder) holder).progressBar.setIndeterminate(true);
        }


    }

    public boolean isLoading(int position) {
        return getItemViewType(position) == VIEW_PROG;

    }

    public class PlayListsViewHolder extends RecyclerView.ViewHolder {

        protected TextView title, link;
        ImageView thumb;

        public PlayListsViewHolder(View v) {
            super(v);

            title = (TextView) v.findViewById(R.id.list_item_title);
            link = (TextView) v.findViewById(R.id.list_item_link);
            thumb = (ImageView) v.findViewById(R.id.list_item_thumb);


        }
    }

    public class ProgressViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public ProgressViewHolder(View v) {
            super(v);
            //	progressBar = (ProgressBar) v.findViewById(R.id.progressBar);
        }
    }
}



