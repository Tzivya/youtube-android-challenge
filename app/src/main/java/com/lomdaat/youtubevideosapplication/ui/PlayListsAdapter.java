package com.lomdaat.youtubevideosapplication.ui;


import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.lomdaat.youtubevideosapplication.R;
import com.lomdaat.youtubevideosapplication.model.Playlist;
import com.lomdaat.youtubevideosapplication.model.Playlists;

public class PlayListsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    public final AppCompatActivity activity;

    Playlists playlistses;
    private final int VIEW_ITEM = 1;
    private final int VIEW_PROG = 0;

    public PlayListsAdapter(AppCompatActivity activity, Playlists mPlayLists) {
        this.activity = activity;
        this.playlistses = mPlayLists;

    }


    public void setPlayLists(Playlists playlists) {
        this.playlistses = playlists;
        notifyDataSetChanged();
    }


    @Override
    public int getItemCount() {
        return playlistses.getPlaylists().size();
    }


    @Override
    public int getItemViewType(int position) {
        return playlistses.getPlaylists().get(position) != null ? VIEW_ITEM : VIEW_PROG;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        RecyclerView.ViewHolder vh;
        View itemView = null;


        itemView = LayoutInflater.
                from(viewGroup.getContext()).
                inflate(R.layout.play_lst_item, viewGroup, false);

        vh = new PlayListsViewHolder(itemView);

        return vh;


    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof PlayListsViewHolder) {
            PlayListsViewHolder playListsViewHolder = ((PlayListsViewHolder) holder);
            final Playlist playlist = playlistses.getPlaylists().get(position);
            playListsViewHolder.mName.setText(playlist.getListTitle());

            playListsViewHolder.playListsItemsAdapter = new PlayListsItemsAdapter(activity, playlist.getListItems());
            playListsViewHolder.playListsRecView.setAdapter(playListsViewHolder.playListsItemsAdapter);


        } else {
            ((ProgressViewHolder) holder).progressBar.setIndeterminate(true);
        }


    }

    public boolean isLoading(int position) {
        return getItemViewType(position) == VIEW_PROG;

    }

    public class PlayListsViewHolder extends RecyclerView.ViewHolder {

        private final LinearLayoutManager linearLayoutManager;
        private final boolean mExpand;
        protected TextView mName;
        public RecyclerView playListsRecView;
        public PlayListsItemsAdapter playListsItemsAdapter;

        public PlayListsViewHolder(View v) {
            super(v);

            mName = (TextView) v.findViewById(R.id.list_item_title);
            playListsRecView = (RecyclerView) itemView.findViewById(R.id.cardList);
            playListsRecView.setHasFixedSize(true);


            linearLayoutManager = new LinearLayoutManager(itemView.getContext(), LinearLayoutManager.HORIZONTAL, false);
            playListsRecView.setLayoutManager(linearLayoutManager);


            mExpand = false;
            //toggleExpandPlayLists();
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    toggleExpandPlayLists();
                }
            });

        }

        private void toggleExpandPlayLists() {
            if (mExpand)
                playListsRecView.setVisibility(View.VISIBLE);
            else
                playListsRecView.setVisibility(View.GONE);
        }
    }

    public class ProgressViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public ProgressViewHolder(View v) {
            super(v);
            //progressBar = (ProgressBar) v.findViewById(R.id.progressBar);
        }
    }
}



