package com.lomdaat.youtubevideosapplication.ui;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.android.volley.VolleyError;
import com.lomdaat.youtubevideosapplication.R;


/**
 * Created by tzivya on 11/1/15.
 */
public class BaseFragment extends Fragment {


    public void showNoConnection() {

        FragmentManager fm = getChildFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        {
            ft.replace(R.id.content_frame, NoConnectionFragment.newInstance(true), "content_frame");
        }
        ft.commit();
    }

    protected void emptyFrame() {

        FragmentManager fm = getChildFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        Fragment fragment = fm.findFragmentByTag("content_frame");
        if (fragment != null) {
            ft.remove(fragment);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
            ft.commit();
        }
    }


    public void showLoading() {


        FragmentManager fm = getChildFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();

        if (fm.getFragments() == null || fm.getFragments().size() == 0) {

            ft.add(R.id.content_frame, new LoadingFragment(), "content_frame");
        } else {
            LoadingFragment loadingFragment = null;
            for (int i = 0; i < fm.getFragments().size(); i++)
                if (fm.getFragments().get(i) instanceof LoadingFragment) {
                    loadingFragment = (LoadingFragment) fm.getFragments().get(i);
                    break;
                }
            if (loadingFragment == null)
                loadingFragment = new LoadingFragment();
            ft.replace(R.id.content_frame, loadingFragment, "content_frame");
        }
        ft.commit();
    }


    public void showContent() {


    }

    public void showError(String message) {

    }


    public void onResponse(String response) {

    }

    public void onErrorResponse(VolleyError volleyError) {

    }


    public void retryRequests() {

    }


}
