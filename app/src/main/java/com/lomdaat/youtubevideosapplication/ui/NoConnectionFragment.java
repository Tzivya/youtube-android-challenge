package com.lomdaat.youtubevideosapplication.ui;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.lomdaat.youtubevideosapplication.R;


public class NoConnectionFragment extends Fragment implements View.OnClickListener {

    public static NoConnectionFragment newInstance(boolean isFragment) {

        Bundle args = new Bundle();

        NoConnectionFragment fragment = new NoConnectionFragment();
        args.putBoolean("isFragment", isFragment);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_no_connection, container, false);
        Button retryBtn = (Button) view.findViewById(R.id.no_connection_retry_btn);
        retryBtn.setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View view) {

        if (R.id.no_connection_retry_btn == view.getId()) {
            if (getArguments() != null
                    && getArguments().getBoolean("isFragment", false))
                ((BaseFragment) getParentFragment()).retryRequests();

        }

    }
}