package com.lomdaat.youtubevideosapplication.ui;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.stream.JsonReader;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.lang.reflect.Type;

public class GsonProvider {
    private static final String TAG = GsonProvider.class.getSimpleName();

    private static GsonProvider sInstance = null;

    private Gson mGson;

    private GsonProvider() {
        mGson = new Gson();

    }

    public static synchronized GsonProvider getInstance() {

        if (sInstance == null) {
            Log.d(TAG, TAG + " created");
            sInstance = new GsonProvider();
        }
        return sInstance;
    }


    public Object parse(String json, Class<?> classOfT) {
        if (json == null || classOfT == null)
            return null;
        JsonReader reader = new JsonReader(new StringReader(json));
        try {
            return mGson.fromJson(reader, classOfT);
        } catch (JsonSyntaxException e) {
            Log.e(TAG, e.getLocalizedMessage(), e);
        }

        return null;

    }

    public Object parse(InputStream inputStream, Class<?> classOfT) {
        if (inputStream == null || classOfT == null)
            return null;

        JsonReader reader = new JsonReader(new InputStreamReader(inputStream));

        try {
            return mGson.fromJson(reader, classOfT);
        } catch (JsonSyntaxException e) {
            Log.e(TAG, e.getLocalizedMessage(), e);
        }

        return null;

    }

    public String toJson(Object object, Class<?> classOfT) {
        if (object == null || classOfT == null)
            return null;

        try {
            return mGson.toJson(object, classOfT);
        } catch (JsonSyntaxException e) {
            Log.e(TAG, e.getLocalizedMessage(), e);
        }

        return null;

    }

    public String toJson(Object object, Type type) {
        if (object == null || type == null)
            return null;

        try {
            mGson.toJson(object, type);
            return mGson.toJson(object, type);
        } catch (JsonSyntaxException e) {
            Log.e(TAG, e.getLocalizedMessage(), e);
        }

        return null;

    }

    public Object parse(String json, Type listType) {
        if (json == null || listType == null)
            return null;

        try {
            return mGson.fromJson(json, listType);
        } catch (JsonSyntaxException e) {
            Log.e(TAG, e.getLocalizedMessage(), e);
        }

        return null;

    }
}
