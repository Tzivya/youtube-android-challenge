package com.lomdaat.youtubevideosapplication;

import android.app.ActivityManager;
import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;


import java.util.List;
import java.util.Locale;


public class YouTubeApplication extends Application
{
    public static final String BASE_URL="http://www.razor-tech.co.il/hiring/youtube-api.json";


    private static YouTubeApplication sInstance;
    public static final int API = android.os.Build.VERSION.SDK_INT;
    private RequestQueue mRequestQueue;



    private static Context sContext;


    @Override
    public void onCreate() {
        super.onCreate();
        sInstance = this;
        mRequestQueue = Volley.newRequestQueue(this);



        sContext = getApplicationContext();






    }

    public synchronized static YouTubeApplication getInstance() {
        return sInstance;
    }

    public RequestQueue getRequestQueue() {
        return mRequestQueue;
    }


    public static Context getContext() {
        return YouTubeApplication.sContext;
    }

}