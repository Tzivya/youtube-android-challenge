
package com.lomdaat.youtubevideosapplication.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "Playlists"
})
public class Playlists {

    @JsonProperty("Playlists")
    private List<Playlist> Playlists = new ArrayList<Playlist>();
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * 
     * @return
     *     The Playlists
     */
    @JsonProperty("Playlists")
    public List<Playlist> getPlaylists() {
        return Playlists;
    }

    /**
     * 
     * @param Playlists
     *     The Playlists
     */
    @JsonProperty("Playlists")
    public void setPlaylists(List<Playlist> Playlists) {
        this.Playlists = Playlists;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
