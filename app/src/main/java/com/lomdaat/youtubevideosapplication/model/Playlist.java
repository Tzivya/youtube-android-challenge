
package com.lomdaat.youtubevideosapplication.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "ListTitle",
    "ListItems"
})
public class Playlist {

    @JsonProperty("ListTitle")
    private String ListTitle;
    @JsonProperty("ListItems")
    private List<ListItem> ListItems = new ArrayList<ListItem>();
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * 
     * @return
     *     The ListTitle
     */
    @JsonProperty("ListTitle")
    public String getListTitle() {
        return ListTitle;
    }

    /**
     * 
     * @param ListTitle
     *     The ListTitle
     */
    @JsonProperty("ListTitle")
    public void setListTitle(String ListTitle) {
        this.ListTitle = ListTitle;
    }

    /**
     * 
     * @return
     *     The ListItems
     */
    @JsonProperty("ListItems")
    public List<ListItem> getListItems() {
        return ListItems;
    }

    /**
     * 
     * @param ListItems
     *     The ListItems
     */
    @JsonProperty("ListItems")
    public void setListItems(List<ListItem> ListItems) {
        this.ListItems = ListItems;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
